Ay'ı hedefleyin. Iska geçseniz bile en azından yıldızlara ulaşırsınız.
Sorunun kendinde olduğunu anlamayan insanlar, çözümü başkalarının huzurunu bozmakta bulur.
Ertelenmiş sözler var dilimde, buruşmuş bir kağıdın içinde duygularım.
İyiyi bilmemek, bize yanlışlar yaptırır.
Önemli olan düşüp düşmemen değil tekrar ayağa kalkıp kalkmamandır.
Gölgesini geçmek isteyen güneşe yürümeyi göze alabilmeli. 
Hayatta üç çeşit insandan korkacaksın: Dağdan inme, dinden dönme, sonradan görme.
Hayatınızın kalitesini, hayatınızdaki insanların kalitesi belirler.
Bir yerin adına denince Türk beldesi, gözüm al bayrak arar, kulağım ezan sesi.
Haksız bir davada zirve olmaktansa, haklı bir davada zerre olmayı tercih ederim.
Herkese iyilik et, bulasın iyilik, herkes ettiğini bulur demişler.
Susmak "Ya Sabır" diyebilmektir. Öyle bir duadır ki yürekte çınlar. Konuşsam dilim yanar. Sussam kalbim.
Yazın gölge hoş kışın çuval boş.
Ney sesleriyle beslenebilseydi serçeler, / Onlar da bir baharın olurlardı bülbülü.
Görevler gereği gibi yapılmazsa, adetler ve kültür bozulur.
Kabiliyetsiz olmak bir kusur değildir. Ama karaktersiz olmak çok büyük bir kusurdur.
Ateşine dayanabileceğin kadar günah işle.
Korkunç olan ölüm değil, yaşanan ya da yaşanamayan hayatlardır.
Allah Kerim'dir amma kuyusu da derindir. İp ve kova olmayınca su çıkmadığı gibi, nur ve feyz de çıkmaz.
Akla bir kere düştü mü düşündüren bir keder, öğütler nafile olur, hayallerinse heder.
İki düşman arasında öyle konuş ki barıştıklarında utanmayasın.
Dostu olmayan insan en yoksul insandır.
En derinde ne duruyorsa o çıkıyor yukarıya en sonunda.
Söz dinleyen kendini duymaz.
"Rabbim! Beni al benden; Ben bende kaldıkça, mahrumum Sen'den."
"Ne kadar dirençli olsa da beden; yürektir bedene hükmeden."
Sevmek, her yaşta güzel, hayat her zaman yaşamaya değer.
Dünyada ve cehennemde yanmanın yolu plajlardan geçer.
Boğulmak için okyanusa gerek yok, bir yudum su yeter.
Ölçüleri yanlış olanların, bütün ölçümleri yanlıştır.
Yaşayabildiklerimiz, eninde sonunda, doğum günlerimizdir, ölüm günlerimiz değil.
Herkes ikinci bir şansı hak eder derler ya hani. Unutma, seven gerçekten sevse, ilk şansını kaybetmezdi.
İnsanların en akıllısı gördüğünden ibret, işittiğinden nasihat alandır.
Şikayet ettiğiniz yaşam, belki de başkasının hayalidir.
Evet, sadece bir yaya gezginim, yeryüzünde bir derviş. Ya siz, siz daha fazla bir şey misiniz?
Sinir bozucu insanlar hep vardı da, internet sağ olsun hepsini bir araya topladı.
Mutluluk varacağımız bir istasyon değil, yolculuk şeklidir.
Hayat dediğin mekan, solup giden yapraktır, çok nazlanma ey insan son mekan kara topraktır.
Bir tane bile düşmana ihtiyaç duymayın bu devirde. Sırtınızdan vuracak onca dostlarınız varken.
Bazen melekler kıskanır masumiyetimizi. Bazen kötülüğümüzü görürde, kaçacak yer arar şeytan.